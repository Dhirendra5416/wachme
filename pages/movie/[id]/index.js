import axios from 'axios';
import { server } from '../../../config';

const Movie = ({movie}) => {
  console.log(movie);
  return (
    <div>Movie Details</div>
  )
}
export async function getStaticProps(){
  const res =await axios(`${server}/634649?api_key=${process.env.API_KEY}&language=en-US&page=1`);
  
  const movie = res.data;

  return{
    props:{movie}
  }
}
export async function getStaticPaths(){
  const res = await axios(`${server}/popular?api_key=${process.env.API_KEY}&language=en-US&page=1`)
  const movies = res.data.results;

  const ids= movies.map(movie => movie.id)
  const paths = ids.map(id =>{{ params: {id:id.toString()}}})

  /*const paths=[
    {params:{id:'121'}},
    {params:{id:'212'}},
    {params:{id:'12'}}
  ] */
  return{
    paths,
    fallback:false
  }
}

export default Movie;