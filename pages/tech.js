import { motion } from "framer-motion"

export default function tech({children,onClick}) {
  return (
    <div className="bg-blue-500 w-full h-screen text-center pt-6">
        <motion.button 
        whileHover={{scale:1.1}}
        whileTap={{scale:0.1}}
        whileDrag={{scaleX:1.2}}
        
        className="text-center rounded-md bg-green-300 text-white px-6 py-6"
        onClick={onClick}>
        tech
        </motion.button>
            <p>This is tech page</p>

        <motion.div className="bg-white-500 w-32 h-32 "
            
            animate={{
            scale: [1, 2, 2, 1, 1],
            rotate: [0, 0, 270, 270, 0],
            borderRadius: ["20%", "20%", "50%", "50%", "20%"],
             }}
        />
        
        </div>
  )
}
