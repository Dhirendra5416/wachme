import React from 'react'
import { Checkbox, Form, InputField, MainButton } from '../../../components/Atom/Form-field'

const NewUser = () => {
  return (
    
    
      <Form>
    <div className='grid grid-cols-2'>
      <InputField id="name" handleChange={"handleChange"}>Name</InputField>
      <InputField id="email" type={"email"}>Email</InputField>
      <MainButton/>
      <div className='flex flex-col items-center'>
       <Checkbox />Male
        <Checkbox/>Female
     </div>

    </div>


      </Form>
      
  
  )
}

export default NewUser