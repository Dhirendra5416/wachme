import Image from 'next/image'
import Link from "next/link"


export default function Hero() {
  return (
      <div className='text-center bg-white-500 pb-2'>
          <div className='w-60 mx-auto'>
              
        <Image src={"/home_cinema.png"} width={50} height={50} layout="responsive"/>
          </div>
          <h1 className='text-2xl text-red-500 font-bold uppercase'>Welcome to watchme</h1>
          <p className='text-xl text-gray-800 font-semibold'>Produce Film and Features, Television Game Video</p>
          <Link href="/Contact">
          <button 
                  
          className='bg-blue-500 rounded text-center py-3 px-6 text-white'>
              Contact
              </button>
          </Link>
      </div>
    
  )
}
