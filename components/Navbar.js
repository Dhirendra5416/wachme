import Link from "next/link"

export default function Navbar() {
   
  return (
     <div className='bg-gray-500 '>
        <div className='container font-bold text-neutral-100 p-4 max-w-7xl mx-auto space-x-8
        tracking-widest font-neue'>
        <Link href="/">
        <a className='text-base md-2xl'> Watch 
            <span className='text-red-800 '>Me</span></a>
        
        </Link>
        <Link href="/tech">
            <a className="text-base md-2xl">Tech</a>
        </Link>
        <Link href={"/item"}>
            Item
        </Link>
        <Link href={"/Registeration/User/NewUser"}>
        <a>NewUser</a>
        
        </Link>
        
        </div>
        </div>
  )
}
