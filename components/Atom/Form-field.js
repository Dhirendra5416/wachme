import PropTypes  from 'prop-types';
export const Form = ({ children, handleSubmit }) => {
    return (
      <div className="overflow-x-auto align-middle min-w-full sm:px-6 lg:px-8">
        <div className="shadow-sm border mt-5 border-cyan-500 sm:rounded-lg bg-white py-4 px-2">
          <form className="min-w-full" onSubmit={(e) => handleSubmit(e)}>
            {children}
          </form>
        </div>
      </div>
    )
  };

 export const InputField = ({
    type,
    id,
    handleChange,
    children,
    defaultValue = '',
   readonly = false,
    required = false,
    }) => {
    return (
      <div className="mx-5 my-3 col-span-6 sm:col-span-6 md:col-span-6 lg:col-span-3 xl:col-span-2 min-w-[1.875rem]">
        <div className="font-sans text-gray font-medium tracking-wide text-sm my-1">
          {children}
        </div>
         <input
            className={`shadow border border-blue-300  rounded-md pl-4 text-gray-dark w-full
             p-2 focus:outline-none focus:ring-0 `}
            type={type}
            id={id}
            name={id}
            onChange={(e) => handleChange(e)}
            defaultValue={defaultValue}
            disabled={readonly}
            required={required}
          />
    
        </div>
    )}
    export const MainButton=({children})=>{
        return(
            <div>
              <button className='rounded-lg bg-white border-black border hover:bg-red-300 text-black px-10 py-4'>{children}</button>
            </div>
        )
    }
export const Checkbox=({
  name,
  id
})=>{
  
  return(
    <div>
      <input className='rounded border-gray-500 px-10 py-10 cursor-pointer'
      type="checkbox"
      id={id}
      name={name}
      />
    </div>
  )
}
